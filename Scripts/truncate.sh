#!/usr/bin/env bash

touch /tmp/lines.txt
echo "one line" > /tmp/lines.txt
echo "another line" > /tmp/lines.txt
# > will constantly overwrite
cat /tmp/lines.txt