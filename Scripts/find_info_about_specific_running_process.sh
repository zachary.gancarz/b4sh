#!/usr/bin/env bash

ps aux | grep nginx # a specific process
ps -ef #all processes
ps aux #alternative format
ps -ef | grep sshd #check a specific process is running