#!/usr/bin/env bash

echo "files created"
touch file1.txt
touch file2.txt
touch file3.txt

echo "files concatenated"
cat file1.txt file2.txt file3.txt > combined_files

echo "files removed"
rm file1.txt file2.txt file3.txt combined_files