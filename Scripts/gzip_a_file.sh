#!/usr/bin/env bash

touch file1.txt
touch file2.txt
touch file3.txt

cat file1.txt file2.txt file3.txt | gzip > combined.gz

rm file1.txt file2.txt file3.txt combined.gz