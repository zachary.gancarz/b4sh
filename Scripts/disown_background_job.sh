#!/usr/bin/env bash

touch hugefile.txt
gzip hugefile.txt &
bg
disown %1
rm hugefile.txt