#!/usr/bin/env bash

kill $(pgrep -f 'python test.py')

# pgrep -f searches the full process name