#!/usr/bin/env bash

tree ./

#to limit depth, use -L

tree ./ -L 2

#to list directories only, use -d

tree ./ -d