#!/usr/bin/env bash

world="world"

echo "Hello \$world"

#\ will also escape the variable to stop it being parsed