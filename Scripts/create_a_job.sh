#!/usr/bin/env bash

#just append a & at the end
echo "start job"
sleep 5 &
notify-send 'title' 'job has been backgrounded'