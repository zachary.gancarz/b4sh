#!/usr/bin/env bash

touch file1.txt
#search for foo
grep foo ./file1.txt
#search for !foo
grep -v foo ./file1.txt
#find all ending in foo
grep "*foo" ./file1.txt

rm file1.txt