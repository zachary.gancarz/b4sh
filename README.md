# b4sh

Handy bash scripts

##### Tips

`-x` enters debug mode. eg: `bash -x hello.sh`

`chmod x hello.sh` makes the file executable

`hexdump hello.sh` dumps the contents as hexadecimal

